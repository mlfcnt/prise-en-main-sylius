# Prise en main Sylius

Brief projet "Sylius"
========================

L'objectif du projet est l'installation d'un framework e-commerce pour découvrir une plateforme de vente de ligne et mettre en oeuvre un composant réutilisable.

# Objectifs pédagogiques

Les savoirs attendus à l'issue du projet :
* Prise en main de sylius
* Créer un plugin sylius

Les compétences visées:
* Compétence 6, développer les composants d'accès aux données : niveau adapter/transposer
* Compétence 8, élaborer et mettre en oeuvre des composants dans une application de gestion de contenu ou e-commerce : niveau adapter/transposer

# Description du projet

Installation de la version standard de Sylius ; une fois l'installation terminée, prenez en main le [back-office de l'application](http://127.0.0.1:8000/admin).
 
Création d'un plugin permettant la diffusion des derniers articles de la boutique sous la forme d'un flux RSS.



Exercices "Sylius"
========================

# Exercice (jour 1)

Création de votre [1er plugin Sylius](https://docs.sylius.com/en/1.5/plugins/creating-plugin.html) :

```php
cd {sylius}
mkdir plugins
cd plugins
composer create-project sylius/plugin-skeleton SyliusMyFirstPlugin
```

Configurer l'autoloader de votre projet sylius pour charger votre plugin :


```
"autoload": {
        "psr-4": {
            "App\\": "src/",
            "Acme\\SyliusExamplePlugin\\": "plugins/SyliusMyFirstPlugin/src/"
        }
    }
```

Ajouter votre plugin au fichier "bundles.php" de votre projet Sylius. Un plugin Sylius est un simple **bundle** Symfony avec une infrastructure intégrée pour les tests utilisant Behat.

Configurer votre application Sylius pour prendre en charge les routes déjà paramétrées dans le squelette de votre plugin et vérifier leur fonctionnement (affichage, chargement du javascript,...).

# Exercice (jour 2)

Création d'une nouvelle fiche produit "Disque dur" ; lorsque ce produit sera disponible sur votre boutique, celui-ci doit pouvoir être commandé avec les options suivantes :

* Capacité : 250GO, 500GO, 1TO
* Couleur : rouge, bleu